module "iam" {
  source = "../modules/iam"
}

module "vpc_main" {
  source             = "../modules/vpc"
  project            = "Conekta-Challenge-Main"
  environment_tag    = "${terraform.workspace}"
  cidr               = "192.168.0.0/16"
  availability_zones = ["us-west-2a", "us-west-2b"]
  public_subnets     = ["192.168.1.0/24", "192.168.2.0/24"]
  private_subnets    = ["192.168.3.0/24", "192.168.4.0/24"]
}

module "vpc_support_01" {
  source             = "../modules/vpc"
  project            = "Conekta-Challenge-Support-01"
  environment_tag    = "${terraform.workspace}"
  cidr               = "10.0.0.0/16"
  availability_zones = ["us-west-2c", "us-west-2d"]
  public_subnets     = ["10.0.1.0/24", "10.0.2.0/24"]
  private_subnets    = ["10.0.3.0/24", "10.0.4.0/24"]
}

module "vpc_support_02" {
  source = "../modules/vpc"
  providers = {
    aws = aws.east
  }
  project            = "Conekta-Challenge-Support-02"
  environment_tag    = "${terraform.workspace}"
  cidr               = "172.31.0.0/16"
  availability_zones = ["us-east-2a", "us-east-2b"]
  public_subnets     = ["172.31.1.0/24", "172.31.2.0/24"]
  private_subnets    = ["172.31.3.0/24", "172.31.4.0/24"]
}


module "bastion" {
  source           = "../modules/ec2"
  instance_name    = "Bastion Server"
  instance_type    = "t2.micro"
  subnet_id        = element(module.vpc_main.subnet_public_ids, 0)
  instance_profile = module.iam.instance_profile_ec2
}

module "alb" {
  source           = "../modules/alb"
  name_alb         = "Alb-Conekta"
  name_tg          = "App-Test"
  subnets_id       = flatten([module.vpc_main.*.subnet_public_ids])
  name_sg          = "SG-Alb"
  vpc_id           = module.vpc_main.vpc_id
  healthcheck_path = "/"
  target_port      = "5000"

}

module "peering" {
  source                 = "../modules/vpc_peering"
  requester_vpc          = module.vpc_main.vpc_id
  accepter_vpc           = module.vpc_support_01.vpc_id
  route_table_id_main    = element(module.vpc_main.route_table_private_id, 0)
  cidr_block_support     = element(module.vpc_support_01.cidr_block_private, 0)
  route_table_id_support = element(module.vpc_support_01.route_table_private_id, 0)
  cidr_block_main        = element(module.vpc_main.cidr_block_private, 0)
}

module "peering_regions" {
  source        = "../modules/vpc_peering_region"
  requester_vpc = module.vpc_support_01.vpc_id
  accepter_vpc  = module.vpc_support_02.vpc_id
}

module "route_us_west_2" {
  source              = "../modules/route"
  route_table_id_main = element(module.vpc_support_01.route_table_private_id, 0)
  cidr_block_support  = element(module.vpc_support_02.cidr_block_private, 0)
  peering_connection  = module.peering_regions.aws_vpc_peering_connection_id
}

module "route_us_east_2" {
  source = "../modules/route"
  providers = {
    aws = aws.east
  }
  route_table_id_main = element(module.vpc_support_02.route_table_private_id, 0)
  cidr_block_support  = element(module.vpc_support_01.cidr_block_private, 0)
  peering_connection  = module.peering_regions.aws_vpc_peering_connection_id
}

module "rds" {
  source                = "../modules/db"
  vpc_id                = module.vpc_main.vpc_id
  sg_ingress_cidr_block = flatten(["${module.vpc_main.cidr_block_public}", "${module.vpc_main.cidr_block_private}"])
  sg_egress_cidr_block  = ["0.0.0.0/0"]
  db_name               = "dbconketa"
  engine                = "postgres"
  engine_version        = "12.3"
  instance_class        = "db.t2.micro"
  subnets_id            = flatten([module.vpc_main.*.subnet_private_ids])
  parameter_group_name  = "default.postgres12"
}

module "ecs" {
  source       = "../modules/ecs"
  cluster_name = "Conekta-Cluster"
}

module "app" {
  source                   = "../modules/app"
  task_name                = "app-test"
  ecs_task_role            = module.iam.ecs_task_role
  ecs_task_execution_role  = module.iam.ecs_task_execution_role
  vpc_id                   = module.vpc_main.vpc_id
  service_name             = "app"
  container_name           = "app-test"
  container_image          = "588664323246.dkr.ecr.us-west-2.amazonaws.com/flask-app:latest"
  container_port           = 5000
  cluster                  = module.ecs.cluster_name
  subnets_id               = flatten([module.vpc_main.*.subnet_private_ids])
  aws_alb_target_group_arn = module.alb.aws_alb_target_group_arn
  ingress_sg               = ["${module.alb.security_group_alb_id}"]
}


