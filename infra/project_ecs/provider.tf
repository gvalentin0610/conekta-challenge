provider "aws" {
  version = "~> 2.57.0"
  region  = "us-west-2"
  profile = "personal"
}

provider "aws" {
  alias   = "east"
  region  = "us-east-2"
  profile = "personal"
}
