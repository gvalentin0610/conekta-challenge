resource "aws_route" "main" {
  route_table_id            = var.route_table_id_main
  destination_cidr_block    = var.cidr_block_support
  vpc_peering_connection_id = var.peering_connection
}
