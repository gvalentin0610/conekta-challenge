variable "requester_vpc" {}

variable "accepter_vpc" {}

variable "route_table_id_main" {}

variable "cidr_block_support" {}

variable "route_table_id_support" {}

variable "cidr_block_main" {}
