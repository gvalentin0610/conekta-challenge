data "aws_caller_identity" "current" {}

resource "aws_vpc_peering_connection" "main2support" {
  vpc_id        = var.requester_vpc
  peer_owner_id = data.aws_caller_identity.current.account_id
  peer_vpc_id   = var.accepter_vpc
  auto_accept   = true
}


resource "aws_route" "main2support" {
  route_table_id            = var.route_table_id_main
  destination_cidr_block    = var.cidr_block_support
  vpc_peering_connection_id = aws_vpc_peering_connection.main2support.id
}

resource "aws_route" "support2main" {
  route_table_id            = var.route_table_id_support
  destination_cidr_block    = var.cidr_block_main
  vpc_peering_connection_id = aws_vpc_peering_connection.main2support.id
}
