data "aws_kms_secrets" "rds" {
  secret {
    name    = "db-password"
    payload = "AQICAHhiJWQ5pIZXQ6qfHpJ1uklbE7V9zi87UDltf7IQH69nEgGIj1nvr1mOR1MB+27BA9yIAAAAbjBsBgkqhkiG9w0BBwagXzBdAgEAMFgGCSqGSIb3DQEHATAeBglghkgBZQMEAS4wEQQM1bgmHsBDVPTM9VSjAgEQgCsgKmaDYqrtc1n3965XFkloPVptiTI+5jqSsczI+yn1jD074S5zeyZsAS3H"
  }
}

data "template_file" "service" {
  template = "${file("../modules/app/task_definitions/service.json.tpl")}"
  vars = {
    password        = data.aws_kms_secrets.rds.plaintext["db-password"]
    container_port  = var.container_port
    container_name  = var.container_name
    container_image = var.container_image
  }
}

resource "aws_security_group" "ecs_tasks" {
  name   = "SG-ECS-Tasks"
  vpc_id = var.vpc_id

  ingress {
    protocol        = "tcp"
    from_port       = var.container_port
    to_port         = var.container_port
    security_groups = var.ingress_sg
  }

  egress {
    protocol    = "-1"
    from_port   = 0
    to_port     = 0
    cidr_blocks = ["0.0.0.0/0"]
  }
}

resource "aws_ecs_task_definition" "main" {
  family                   = var.task_name
  network_mode             = "awsvpc"
  requires_compatibilities = ["FARGATE"]
  cpu                      = 256
  memory                   = 512
  execution_role_arn       = var.ecs_task_execution_role
  task_role_arn            = var.ecs_task_role
  container_definitions    = data.template_file.service.rendered
}

resource "aws_ecs_service" "main" {
  name                               = var.service_name
  cluster                            = var.cluster
  task_definition                    = aws_ecs_task_definition.main.arn
  desired_count                      = 2
  deployment_minimum_healthy_percent = 50
  deployment_maximum_percent         = 200
  launch_type                        = "FARGATE"
  scheduling_strategy                = "REPLICA"

  network_configuration {
    security_groups  = ["${aws_security_group.ecs_tasks.id}"]
    subnets          = var.subnets_id
    assign_public_ip = false
  }

  load_balancer {
    target_group_arn = var.aws_alb_target_group_arn
    container_name   = var.container_name
    container_port   = var.container_port
  }

  lifecycle {
    ignore_changes = [task_definition, desired_count]
  }
}
