variable "vpc_id" {}

variable "container_port" {}

variable "ecs_task_role" {}

variable "ecs_task_execution_role" {}

variable "container_name" {}

variable "container_image" {}

variable "service_name" {}

variable "subnets_id" {}

variable "cluster" {}

variable "aws_alb_target_group_arn" {}

variable "task_name" {}

variable "ingress_sg" {}
