[
    {
        "volumesFrom": [],
        "memory": 512,
        "portMappings": [
            {
                "hostPort": ${container_port
                },
                "containerPort": ${container_port
                },
                "protocol": "tcp"
            }
        ],
        "essential": true,
        "mountPoints": [],
        "name": "${container_name}",
        "environment": [
            {
                "name": "POSTGRES_DB",
                "value": "dbconketa"
            },
            {
                "name": "POSTGRES_HOST",
                "value": "terraform-20201110021832061000000001.c9ghw7ccwbdk.us-west-2.rds.amazonaws.com"
            },
            {
                "name": "POSTGRES_PORT",
                "value": "5432"
            },
            {
                "name": "POSTGRES_USER",
                "value": "gvalentin"
            }
        ],
        "image": "${container_image}",
        "cpu": 2,
        "secrets": [
            {
                "name": "POSTGRES_PASSWORD",
                "valueFrom": "arn:aws:ssm:us-west-2:588664323246:parameter/db_password"
            }
        ]
    }
]