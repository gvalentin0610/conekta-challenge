data "aws_caller_identity" "current" {}

resource "aws_vpc_peering_connection" "region" {
  peer_owner_id = data.aws_caller_identity.current.account_id
  peer_vpc_id   = var.accepter_vpc
  vpc_id        = var.requester_vpc
  peer_region   = "us-east-2"
}
