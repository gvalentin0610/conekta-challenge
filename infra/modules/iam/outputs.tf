output "role_ec2_arn" {
  value = aws_iam_role.ec2_role.arn
}

output "instance_profile_ec2" {
  value = aws_iam_instance_profile.ec2_profile.name
}

output "ecs_task_role" {
  value = aws_iam_role.ecs_task_role.arn
}

output "ecs_task_execution_role" {
  value = aws_iam_role.ecs_task_execution_role.arn
}
