data "aws_kms_secrets" "rds" {
  secret {
    name    = "db-password"
    payload = "AQICAHhiJWQ5pIZXQ6qfHpJ1uklbE7V9zi87UDltf7IQH69nEgGIj1nvr1mOR1MB+27BA9yIAAAAbjBsBgkqhkiG9w0BBwagXzBdAgEAMFgGCSqGSIb3DQEHATAeBglghkgBZQMEAS4wEQQM1bgmHsBDVPTM9VSjAgEQgCsgKmaDYqrtc1n3965XFkloPVptiTI+5jqSsczI+yn1jD074S5zeyZsAS3H"
  }
}

resource "aws_db_subnet_group" "main" {
  name       = "db_subnet"
  subnet_ids = var.subnets_id

  tags = {
    Name = "DB subnet group"
  }
}

resource "aws_security_group" "rds" {
  name = "SG-RDS"

  description = "RDS"
  vpc_id      = var.vpc_id

  ingress {
    from_port   = "5432"
    to_port     = "5432"
    protocol    = "tcp"
    cidr_blocks = var.sg_ingress_cidr_block
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = var.sg_egress_cidr_block
  }
}

resource "aws_db_instance" "main" {
  allocated_storage      = 20
  storage_type           = "gp2"
  engine                 = var.engine
  engine_version         = var.engine_version
  instance_class         = var.instance_class
  name                   = var.db_name
  username               = "gvalentin"
  password               = data.aws_kms_secrets.rds.plaintext["db-password"]
  parameter_group_name   = var.parameter_group_name
  db_subnet_group_name   = aws_db_subnet_group.main.name
  vpc_security_group_ids = ["${aws_security_group.rds.id}"]
}
