variable "engine" {}

variable "engine_version" {}

variable "instance_class" {}

variable "db_name" {}

variable "subnets_id" {}

variable "parameter_group_name" {}

variable "vpc_id" {}

variable "sg_ingress_cidr_block" {}

variable "sg_egress_cidr_block" {}
