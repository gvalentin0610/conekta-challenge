variable "cidr" {}

variable "availability_zones" {}

variable "private_subnets" {}

variable "public_subnets" {}

variable "project" {}

variable "environment_tag" {}
