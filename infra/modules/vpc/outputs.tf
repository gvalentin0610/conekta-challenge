output "subnet_public_ids" {
  value = aws_subnet.public.*.id
}

output "subnet_private_ids" {
  value = aws_subnet.private.*.id
}

output "vpc_id" {
  value = aws_vpc.main.id
}

output "route_table_public_id" {
  value = aws_route_table.public.id
}

output "route_table_private_id" {
  value = aws_route_table.private.*.id
}

output "route_table_main_id" {
  value = aws_vpc.main.*.main_route_table_id
}

output "cidr_block" {
  value = aws_vpc.main.cidr_block
}

output "cidr_block_public" {
  value = aws_subnet.public.*.cidr_block
}

output "cidr_block_private" {
  value = aws_subnet.private.*.cidr_block
}
