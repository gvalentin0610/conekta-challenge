data "aws_acm_certificate" "issued" {
  domain   = "*.gvproject.co"
  statuses = ["ISSUED"]
}

resource "aws_security_group" "alb" {
  name   = var.name_sg
  vpc_id = var.vpc_id

  ingress {
    protocol    = "tcp"
    from_port   = 80
    to_port     = 80
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    protocol    = "tcp"
    from_port   = 443
    to_port     = 443
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    protocol    = "-1"
    from_port   = 0
    to_port     = 0
    cidr_blocks = ["0.0.0.0/0"]

  }
}

resource "aws_lb" "main" {
  name               = var.name_alb
  internal           = false
  load_balancer_type = "application"
  security_groups    = [aws_security_group.alb.id]
  subnets            = var.subnets_id

  enable_deletion_protection = false
}

resource "aws_alb_target_group" "main" {
  name        = var.name_tg
  port        = 5000
  protocol    = "HTTP"
  vpc_id      = var.vpc_id
  target_type = "ip"

  health_check {
    healthy_threshold   = "2"
    interval            = "70"
    protocol            = "HTTP"
    matcher             = "200"
    timeout             = "65"
    path                = var.healthcheck_path
    port                = var.target_port
    unhealthy_threshold = "2"
  }
}

resource "aws_alb_listener" "http" {
  load_balancer_arn = aws_lb.main.id
  port              = 80
  protocol          = "HTTP"

  default_action {
    type = "redirect"

    redirect {
      port        = 443
      protocol    = "HTTPS"
      status_code = "HTTP_301"
    }
  }
}

resource "aws_alb_listener" "https" {
  load_balancer_arn = aws_lb.main.id
  port              = 443
  protocol          = "HTTPS"

  ssl_policy      = "ELBSecurityPolicy-2016-08"
  certificate_arn = data.aws_acm_certificate.issued.arn
  default_action {
    target_group_arn = aws_alb_target_group.main.id
    type             = "forward"
  }
}
