# conekta-challenge

Conekta Challenge

1.- You need to configure your AWS credentials an configure a profile called `personal` in the folder `$HOME/.aws`

2.- Download the repo cd infra/project_ecs

3.- Create a terraform workspace depending the environment `terraform workspace new dev` and `terraform workspace select dev`

4.- `terraform plan`

5.- `terrafom apply`